package br.ucsal.bes20201.testequalidade.locadora;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuider;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o calculo do valor de locacao por 3 dias para 2 veiculos com 1 ano de
	 * fabricacao e 3 veiculos com 8 anos de fabricacao.
	 */
	
	/* 	#	|	 ENTRADA DE CADA VE�CULO		| 	  SA�DA ESPERADA
	 *  1   | valor di�ria = 62.65, ano = 2019 	|		  187.95
	 *  2   | valor di�ria = 57.54, ano = 2019 	|		  172.62
	 *  3   | valor di�ria = 39.00, ano = 2012 	|		   105.3
	 *  4   | valor di�ria = 33.30, ano = 2012 	|		   89.91
	 *  5   | valor di�ria = 32.00, ano = 2012 	|		    86.4
	 * 
	 * 		SA�DA ESPERADA PARA O VALOR TOTAL DE LOCA��O = 642.18
	 */
	
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
		VeiculoBuider veiculoBuider = VeiculoBuider.umCarro().deModelo(new Modelo("Hyundai HB20S")).doAno(2019).comValorDiaria(62.65);
		
		Veiculo veiculo1 = veiculoBuider.build();
		Veiculo veiculo2 = veiculoBuider.mas().deModelo(new Modelo("Renault Logan")).comValorDiaria(57.54).build();
		Veiculo veiculo3 = veiculoBuider.mas().deModelo(new Modelo("Fiat Novo Uno")).doAno(2012).comValorDiaria(39.00).build();
		Veiculo veiculo4 = veiculoBuider.mas().deModelo(new Modelo("Chevrolet Celta")).doAno(2012).comValorDiaria(33.30).build();
		Veiculo veiculo5 = veiculoBuider.mas().deModelo(new Modelo("Chevrolet Corsa")).doAno(2012).comValorDiaria(32.00).build();
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
		
		Double resultadoEsperado = 642.18;
		Double resultadoAtual = LocacaoBO.calcularValorTotalLocacao(veiculos, 3);
		
		Assertions.assertEquals(resultadoEsperado, resultadoAtual);
		
	}
}
