package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuider {

	private static final String PLACA_DEFAULT = "BRA0S17";
	private static final Integer ANO_DEFAULT = 2016;
	private static final Modelo MODELO_DEFAULT = new Modelo("Volkswagen Gol");
	private static final Double VALOR_DIARIA_DEFAULT = 41.92;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa = PLACA_DEFAULT;
	private Integer ano = ANO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT;
	private Double valorDiaria = VALOR_DIARIA_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;

	private VeiculoBuider() {

	}

	public static VeiculoBuider umCarro() {
		return new VeiculoBuider();
	}

	public VeiculoBuider comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuider doAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuider deModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuider comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuider comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuider mas() {
		return umCarro().comPlaca(placa).doAno(ano).deModelo(modelo).comValorDiaria(valorDiaria).comSituacao(situacao);

	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}

}
